#!/bin/bash
if [ -z $1 ]
then
  dataset=
else
  dataset=$1
fi

matchCompression="[0-9]+\.[0-9]{2}x"
match1Gb="[0-9]{1,}(,[0-9]+)?(G|T)"
match10Gb="[0-9]{2,}(,[0-9]+)?(G|T)"
matchAnySize="(\s0\s|[0-9]+(,[0-9]+)?.?)"

#Check if snapshot itself uses more than 1GB of data
match1="$match1Gb\s+$matchCompression"
#match1=""

#check if snapshot references more than 10GB of data
match2="$match10Gb\s+$matchAnySize\s+$matchCompression"
match2=""

#Check if more than 1GB new data was written to this snapshot
match3="$match1Gb\s+$matchAnySize\s+$matchAnySize\s+$matchCompression"
#match3=""

list=`zfs list -t all -r -o creation,type,name,written,referenced,used,compressratio,logicalreferenced,lused $dataset`
firstLine=`echo -e "$list" | head -n1`

echo -e "$list" | sed "/\sfilesystem\s/i\ " | sed "/\sfilesystem\s/i$firstLine" | sed "/\sfilesystem\s/a\ " | grep --color -E "^|$match1|$match2|$match3"

echo ""
echo "WRITTEN: New data written since last snapshot. If 10 GB is written and 9 GB is deleted. it will say 10G"
echo "REFER: Data referenced in this snaphsot"
echo "USED: Data referenced only by this snapshot (freed on deleting this snapshot)"
echo "RATIO: Compression Ratio"
echo "LREFER/LUSED: Uncompressed REFER/USED"
echo ""
echo "Note that if data is referenced by two or more snapshots it will only be freed if all snapshots are deleted. The USED column only shows data that is referenced by only this snapshot. Check the REFER Column after a big WRITE to find short lived data that spans multiple snapshots"
